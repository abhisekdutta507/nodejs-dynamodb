const dynamoose = require("dynamoose");
var Schema = dynamoose.Schema;

const UserSchema = new Schema(
  {
    email: {
      type: String
    },
    name: {
      type: String,
      index: {
        global: true,
        name: "name-index"
      }
    },
    createdAt: {
      type: Date,
      default: Date.now()
    },
    id: {
      type: String
    }
  },
  {
    throughput: { read: 5, write: 5 }
  }
);

module.exports = dynamoose.model("users", UserSchema);
