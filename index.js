const express = require("express");
const dynamoose = require("dynamoose");
const bodyParser = require("body-parser");
const AWS = require('aws-sdk');
const config = require('./config/config');
const { v4: uuidv4 } = require('uuid');
const app = express();
const Users = require('./model/users.model');

const ddb = new dynamoose.aws.sdk.DynamoDB(config.aws_remote_config);
dynamoose.aws.ddb.set(ddb);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.get('/getMovies/:email', (req, res) => {
  AWS.config.update(config.aws_remote_config);

  const docClient = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: config.aws_table_name,
    // IndexName: "email",
    KeyConditionExpression: "email = :v_email",
    ExpressionAttributeValues: {
      ":v_email": req.params.email
    },
    // ProjectionExpression: "id, createdAt, name, email"
  };

  docClient.query(params, function (err, data) {

    if (err) {
      console.log(err)
      res.send({
        success: false,
        message: err
      });
    } else {
      const { Items } = data;
      res.send({
        success: true,
        Items
      });
    }
  });
})

app.post('/addUser', (req, res) => {
  AWS.config.update(config.aws_remote_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const Item = { ...req.body };
  Item.id = uuidv4();
  Item.createdAt = Date.now();
  var params = {
    TableName: config.aws_table_name,
    Item: Item
  };
  // Call DynamoDB to add the item to the table
  docClient.put(params, function (err, data) {
    if (err) {
      res.send({
        success: false,
        message: err
      });
    } else {
      res.send({
        success: true,
        movie: data
      });
    }
  });
})

app.get('/search/:name', async (req, res) => {
  Users.query('name').eq(req.params.name).sort('ascending').exec((error, data) => {
    if (error) {
      return res.status(404).send({ error: true, data: error });
    }
    return res.status(200).send({ error: false, data });
  })
});

app.listen(3000, console.log("Server started at 4001"));
